﻿//Author: Aaftab Khanna, Group 3: Najeam, Darshan 
using System.Windows;

namespace GotADollar
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        decimal DisplayResult;
        decimal MissResult;
        CalcSum calc;
        public MainWindow()
        {
            InitializeComponent();
            calc = new CalcSum();
            DataContext = calc; //Tells XAML where to find data from
        }

        private void Calculate_Click(object sender, RoutedEventArgs e)
        {
            DisplayResult = calc.Add();
            if (DisplayResult == 100)
            {
                MessageBox.Show("Congratulations you have won the game. More exciting Levels coming soon !");
            }
            else if (DisplayResult > 100)
            {
                MissResult = DisplayResult - 100;
                MessageBox.Show("OOPS you are " + MissResult + " cents over !");
            }
            else if (DisplayResult < 100 && DisplayResult > 0)
            {
                MissResult = 100 - DisplayResult;
                MessageBox.Show("OOPS you miss it by " + MissResult + " cents !");
            }
            else if (DisplayResult < 0)
            {
                MessageBox.Show("Please enter a Valid number of coins.");
                DisplayResult = 0;
            }
        }
    }
}
