﻿//Author: Aaftab Khanna, Group 3: Najeam, Darshan 
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace GotADollar
{
    class CalcSum : INotifyPropertyChanged
    {
        decimal _penny;
        decimal _nickel;
        decimal _dime;
        decimal _quarter;
        decimal _result;
        public decimal Penny
        {
            get { return _penny; }
            set { _penny = value; NotifyPropertyChanged(); }
        }
        public decimal Nickel
        {
            get { return _nickel; }
            set { _nickel = value; NotifyPropertyChanged(); }
        }
        public decimal Dime
        {
            get { return _dime; }
            set { _dime = value; NotifyPropertyChanged(); }
        }
        public decimal Quarter
        {
            get { return _quarter; }
            set { _quarter = value; NotifyPropertyChanged(); }
        }
        public decimal Result
        {
            get { return _result; }
            set { _result = value; NotifyPropertyChanged(); }
        }
        public decimal Add()
        {
            Result = (_penny) * 1 + (_nickel) * 5 + (_dime) * 10 + (_quarter) * 25;
            return Result;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
