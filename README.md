**Getting Started**

Open .cs file
Clean the solution
Build it using Ctrl + Shift + B
Run using Ctrl + F5


**The Right License - MIT**

The MIT License is very permissive. It is not only welcoming to open-source developers, but also to businesses. This quality of the license allows it to be both business-friendly and open-source friendly, while still making it possible to be monetized.

As the MIT license is permitted for the closed source as well, it makes it a great license for commercial purposes. As the license is so permissive, the code is used by many people to develop third-party software. The major concern amongst various developers, however, is regarding the monetary return of the code protected under this license.


